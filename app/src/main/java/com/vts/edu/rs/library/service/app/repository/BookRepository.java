package com.vts.edu.rs.library.service.app.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BookRepository extends CrudRepository<BookEntity, String> {

    Optional<BookEntity> findByBookId(String bookId);
}
