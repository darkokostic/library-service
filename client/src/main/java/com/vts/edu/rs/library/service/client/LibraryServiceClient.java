package com.vts.edu.rs.library.service.client;

import com.vts.edu.rs.library.service.api.libraries.model.Book;
import com.vts.edu.rs.library.service.api.libraries.model.GetLibrariesResponse;
import com.vts.edu.rs.library.service.api.libraries.model.Library;
import com.vts.edu.rs.library.service.api.libraries.model.LibraryResponse;

import javax.validation.Valid;

public interface LibraryServiceClient {

    LibraryResponse getLibraryById(String libraryId) throws LibraryServiceClientException;

    GetLibrariesResponse getLibraries() throws LibraryServiceClientException;

    LibraryResponse createLibrary(@Valid Library library) throws LibraryServiceClientException;

    void addBook(String libraryId, @Valid Book book) throws LibraryServiceClientException;

    GetLibrariesResponse searchByBookTitle(String bookTitle) throws LibraryServiceClientException;
}
