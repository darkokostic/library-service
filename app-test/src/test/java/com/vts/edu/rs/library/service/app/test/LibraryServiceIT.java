package com.vts.edu.rs.library.service.app.test;

import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.authors.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.client.BooksServiceClient;
import com.vts.edu.rs.books.service.client.BooksServiceRestClient;
import com.vts.edu.rs.library.service.api.libraries.model.GetLibrariesResponse;
import com.vts.edu.rs.library.service.api.libraries.model.Library;
import com.vts.edu.rs.library.service.api.libraries.model.LibraryResponse;
import com.vts.edu.rs.library.service.client.LibraryServiceClient;
import com.vts.edu.rs.library.service.client.LibraryServiceRestClient;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.DockerComposeContainer;

import java.io.File;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

public class LibraryServiceIT {

    private static final String TEST_COMPOSE_FILE_URL = "src/test/resources/test-compose.yml";

    private static final String LIBRARY_SERVICE_NAME = "library-service";

    private static final String LIBRARY_SERVICE_BASE_URL = "http://localhost:8888";

    private static final String BOOK_SERVICE_BASE_URL = "http://localhost:8080";

    @ClassRule
    public static DockerComposeContainer DOCKER_COMPOSE_SETUP = new DockerComposeContainer(
                new File(TEST_COMPOSE_FILE_URL))
                .withLocalCompose(true)
                .withExposedService(LIBRARY_SERVICE_NAME, 8080);

    private LibraryServiceClient libraryServiceClient;

    private BooksServiceClient booksServiceClient;

    @Before
    public void setup() {
        libraryServiceClient = createLibraryServiceClient();
        booksServiceClient = createBookServiceClient();
    }

    @Test
    public void createLibraryWorksAsExpected() throws Exception {
        Library library = createLibraryModel();
        LibraryResponse libraryResponse = libraryServiceClient.createLibrary(library);
        assertLibrary(library, libraryResponse);
    }

    @Test
    public void getLibrariesWorksAsExpected() throws Exception {
        Library library = createLibraryModel();
        libraryServiceClient.createLibrary(library);

        GetLibrariesResponse response = libraryServiceClient.getLibraries();
        List<LibraryResponse> libraries = response.getLibraries();
        assertFalse(libraries.isEmpty());
    }

    @Test
    public void getLibraryByIdWorksAsExpected() throws Exception {
        Library library = createLibraryModel();
        LibraryResponse createdLibrary = libraryServiceClient.createLibrary(library);

        LibraryResponse retrievedLibrary = libraryServiceClient.getLibraryById(createdLibrary.getId());
        assertLibrary(library, retrievedLibrary);
    }

    @Test
    public void searchLibrariesThatContainsBookWorksAsExpected() throws Exception {
        Author author = createAuthorModel("Author 1", "author1@demo.com");
        AuthorResponse authorResponse = booksServiceClient.createAuthor(author);

        Book book = createBookModel(authorResponse.getId(), "isbn1");
        BookResponse bookResponse = booksServiceClient.createBook(book);

        Library library = createLibraryModel();
        library.setBooks(singletonList(new com.vts.edu.rs.library.service.api.libraries.model.Book().id(bookResponse.getId())));
        LibraryResponse libraryResponse = libraryServiceClient.createLibrary(library);

        GetLibrariesResponse librariesResponse = libraryServiceClient.searchByBookTitle("Book1");
        assertNotNull(librariesResponse);

        List<LibraryResponse> librariesContainingBook = librariesResponse.getLibraries();
        assertFalse(librariesContainingBook.isEmpty());
        assertLibraryResponse(libraryResponse, librariesContainingBook);
    }

    @Test
    public void addBookToLibraryWorksAsExpected() throws Exception {
        Author author = createAuthorModel("Author 2", "author2@demo.com");
        AuthorResponse authorResponse = booksServiceClient.createAuthor(author);

        Book book = createBookModel(authorResponse.getId(), "isbn2");
        BookResponse bookResponse = booksServiceClient.createBook(book);

        Library library = createLibraryModel();
        LibraryResponse libraryResponse = libraryServiceClient.createLibrary(library);

        libraryServiceClient.addBook(libraryResponse.getId(), new com.vts.edu.rs.library.service.api.libraries.model.Book().id(bookResponse.getId()));

        GetLibrariesResponse librariesResponse = libraryServiceClient.searchByBookTitle("Book1");
        assertNotNull(librariesResponse);

        List<LibraryResponse> librariesContainingBook = librariesResponse.getLibraries();
        assertFalse(librariesContainingBook.isEmpty());
        assertLibraryResponse(libraryResponse, librariesContainingBook);
    }

    private LibraryServiceClient createLibraryServiceClient() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate restTemplate = restTemplateBuilder.build();
        return new LibraryServiceRestClient(LIBRARY_SERVICE_BASE_URL, restTemplate);
    }

    private BooksServiceClient createBookServiceClient() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate restTemplate = restTemplateBuilder.build();
        return new BooksServiceRestClient(BOOK_SERVICE_BASE_URL, restTemplate);
    }

    private Library createLibraryModel() {
        Library library = new Library();
        library.setName("Library 1");
        library.setAddress("Library 1 Address");
        return library;
    }

    private Author createAuthorModel(String name, String email) {
        Author author = new Author();
        author.setName(name);
        author.setEmail(email);
        author.setBio("Bio");
        return author;
    }

    private Book createBookModel(String authorId, String isbn) {
        Book book = new Book();
        book.setTitle("Book1");
        book.setIsbn(isbn);
        book.setIssuer("Issuer");
        book.setPagesNumber(234);
        book.setYearOfIssue(2007);
        book.setAuthorId(authorId);
        return book;
    }

    private void assertLibrary(Library library, LibraryResponse libraryResponse) {
        assertNotNull(libraryResponse);
        assertEquals(library.getName(), libraryResponse.getName());
        assertEquals(library.getAddress(), libraryResponse.getAddress());
        assertEquals(Integer.valueOf(0), libraryResponse.getNumberOfBooks());
    }

    private void assertLibraryResponse(LibraryResponse libraryResponse, List<LibraryResponse> libraries) {
        LibraryResponse library = libraries.get(0);
        assertEquals(libraryResponse.getId(), library.getId());
        assertEquals(libraryResponse.getName(), library.getName());
        assertEquals(libraryResponse.getAddress(), library.getAddress());
        assertEquals(Integer.valueOf(1), library.getNumberOfBooks());
    }
}
