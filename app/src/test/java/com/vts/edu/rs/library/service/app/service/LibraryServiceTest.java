package com.vts.edu.rs.library.service.app.service;

import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.library.service.api.libraries.model.Book;
import com.vts.edu.rs.library.service.api.libraries.model.Library;
import com.vts.edu.rs.library.service.api.libraries.model.LibraryResponse;
import com.vts.edu.rs.library.service.app.exceptions.NotFoundException;
import com.vts.edu.rs.library.service.app.repository.BookEntity;
import com.vts.edu.rs.library.service.app.repository.LibraryEntity;
import com.vts.edu.rs.library.service.app.support.AbstractLibraryServiceTestCase;
import org.junit.Test;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

public class LibraryServiceTest extends AbstractLibraryServiceTestCase {

    @Test
    public void createLibraryWorksAsExpected() {
        Library library = new Library();
        library.setName("Library");
        library.setAddress("Address");

        LibraryResponse libraryResponse = libraryService.createLibrary(library);
        assertLibrary(library, libraryResponse);
    }

    @Test
    public void getLibrariesWorksAsExpected() {
        LibraryEntity libraryEntity = saveLibraryEntity("Library", "Address", singletonList(new BookEntity("book-id")));

        List<LibraryResponse> libraries = libraryService.getLibraries();
        assertFalse(libraries.isEmpty());

        LibraryResponse libraryResponse = libraries.get(0);
        assertLibrary(libraryEntity, libraryResponse);
    }

    @Test
    public void getLibraryByIdWorksAsExpected() {
        LibraryEntity libraryEntity = saveLibraryEntity("Library", "Address", singletonList(new BookEntity("book-id")));

        LibraryResponse libraryResponse = libraryService.getLibraryById(libraryEntity.getId());
        assertLibrary(libraryEntity, libraryResponse);
    }

    @Test(expected = NotFoundException.class)
    public void getLibraryByIdWithWrongIdThrowsException() {
        libraryService.getLibraryById("wrong-library-id");
    }

    @Test
    public void addBookToLibraryWorksAsExpected() {
        LibraryEntity libraryEntity = saveLibraryEntity("Library", "Address", emptyList());

        // Saving book to books-service
        BookResponse createdBook = saveBookToBookService("book-id");

        // Adding book to created library
        libraryService.addBook(libraryEntity.getId(), new Book().id(createdBook.getId()));

        LibraryResponse libraryResponse = libraryService.getLibraryById(libraryEntity.getId());
        assertEquals(Integer.valueOf(1), libraryResponse.getNumberOfBooks());
    }

    @Test(expected = HttpClientErrorException.class)
    public void addBookToLibraryWithWrongIdThrowsException() {
        LibraryEntity libraryEntity = saveLibraryEntity("Library", "Address", emptyList());
        // Throws exception as we try to add book which does not exist to library
        libraryService.addBook(libraryEntity.getId(), new Book().id("book-id"));
    }

    @Test
    public void searchLibrariesByBookWorksAsExpected() {
        // Save book to book-service
        BookResponse createdBook = saveBookToBookService("book-id", "Book Title");

        // Save library to library-service that contains the book with book id
        LibraryEntity libraryEntity = saveLibraryEntity("Library", "Address",
                singletonList(new BookEntity(createdBook.getId())));

        List<LibraryResponse> libraries = libraryService.searchByBookTitle(createdBook.getTitle());
        assertFalse(libraries.isEmpty());

        LibraryResponse libraryResponse = libraries.get(0);
        assertLibrary(libraryEntity, libraryResponse);
    }
}
