package com.vts.edu.rs.library.service.app.fakes;

import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.authors.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.authors.model.AuthorSearchParameters;
import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.api.books.model.BookSearchParameters;
import com.vts.edu.rs.books.service.api.books.model.GetBooksResponse;
import com.vts.edu.rs.books.service.client.BooksServiceClient;
import com.vts.edu.rs.books.service.client.BooksServiceClientException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("RedundantThrows")
public class BookServiceClientFake implements BooksServiceClient {

    private final List<BookResponse> bookResponses = new ArrayList<>();

    public void addBook(BookResponse bookResponse) {
        bookResponses.add(bookResponse);
    }

    public void clear() {
        bookResponses.clear();
    }

    @Override
    public AuthorResponse getAuthorById(String authorId) throws BooksServiceClientException {
        throw new NotImplementedException();
    }

    @Override
    public List<AuthorResponse> getAuthors() throws BooksServiceClientException {
        throw new NotImplementedException();
    }

    @Override
    public List<AuthorResponse> searchAuthors(AuthorSearchParameters authorSearchParameters) throws BooksServiceClientException {
        throw new NotImplementedException();
    }

    @Override
    public AuthorResponse createAuthor(Author author) throws BooksServiceClientException {
        throw new NotImplementedException();
    }

    @Override
    public BookResponse createBook(@Valid Book book) throws BooksServiceClientException {
        throw new NotImplementedException();
    }

    @Override
    public BookResponse getBookById(String bookId) throws BooksServiceClientException {
        return bookResponses.stream()
                .filter(book -> bookId.equals(book.getId()))
                .findFirst()
                .orElseThrow(() -> new BooksServiceClientException(new RuntimeException()));
    }

    @Override
    public GetBooksResponse searchBooks(BookSearchParameters bookSearchParameters) throws BooksServiceClientException {
        List<BookResponse> searchedBooks = bookResponses.stream().filter(bookResponse -> bookSearchParameters.getBookTitle().equals(bookResponse.getTitle()))
                .collect(Collectors.toList());
        return new GetBooksResponse()
                .books(searchedBooks);
    }

    @Override
    public GetBooksResponse getBooks() throws BooksServiceClientException {
        throw new NotImplementedException();
    }
}
