package com.vts.edu.rs.library.service.client;

@SuppressWarnings("WeakerAccess")
public class LibraryServiceClientException extends Exception {

    public LibraryServiceClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public LibraryServiceClientException(Throwable cause) {
        super(cause);
    }
}
