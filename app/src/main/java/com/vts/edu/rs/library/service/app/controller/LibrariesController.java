package com.vts.edu.rs.library.service.app.controller;

import com.vts.edu.rs.library.service.api.libraries.LibrariesApi;
import com.vts.edu.rs.library.service.api.libraries.model.Book;
import com.vts.edu.rs.library.service.api.libraries.model.GetLibrariesResponse;
import com.vts.edu.rs.library.service.api.libraries.model.Library;
import com.vts.edu.rs.library.service.api.libraries.model.LibraryResponse;
import com.vts.edu.rs.library.service.app.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class LibrariesController implements LibrariesApi {

    @Autowired
    private final LibraryService libraryService;

    public LibrariesController(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    @Override
    public ResponseEntity<LibraryResponse> createLibrary(@Valid Library library) {
        LibraryResponse libraryResponse = libraryService.createLibrary(library);
        return ResponseEntity.ok(libraryResponse);
    }

    @Override
    public ResponseEntity<GetLibrariesResponse> getLibraries() {
        List<LibraryResponse> libraries = libraryService.getLibraries();
        GetLibrariesResponse librariesResponse = new GetLibrariesResponse();
        librariesResponse.setLibraries(libraries);
        return ResponseEntity.ok(librariesResponse);
    }

    @Override
    public ResponseEntity<LibraryResponse> getLibraryById(String libraryId) {
        LibraryResponse libraryResponse = libraryService.getLibraryById(libraryId);
        return ResponseEntity.ok(libraryResponse);
    }

    @Override
    public ResponseEntity<Void> addBook(String libraryId, @Valid Book book) {
        libraryService.addBook(libraryId, book);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<GetLibrariesResponse> searchByBookTitle(String bookTitle) {
        List<LibraryResponse> libraries = libraryService.searchByBookTitle(bookTitle);
        GetLibrariesResponse librariesResponse = new GetLibrariesResponse();
        librariesResponse.setLibraries(libraries);
        return ResponseEntity.ok(librariesResponse);
    }
}
