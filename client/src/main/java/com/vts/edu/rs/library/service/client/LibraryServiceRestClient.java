package com.vts.edu.rs.library.service.client;

import com.vts.edu.rs.library.service.api.libraries.model.Book;
import com.vts.edu.rs.library.service.api.libraries.model.GetLibrariesResponse;
import com.vts.edu.rs.library.service.api.libraries.model.Library;
import com.vts.edu.rs.library.service.api.libraries.model.LibraryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;

@SuppressWarnings("unused")
public class LibraryServiceRestClient implements LibraryServiceClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryServiceRestClient.class);

    private final String libraryServiceBaseUrl;

    private final RestTemplate restTemplate;

    public LibraryServiceRestClient(String libraryServiceBaseUrl, RestTemplate restTemplate) {
        this.libraryServiceBaseUrl = libraryServiceBaseUrl;
        this.restTemplate = restTemplate;
    }

    @Override
    public LibraryResponse getLibraryById(String libraryId) throws LibraryServiceClientException {
        String url = libraryServiceBaseUrl + "/libraries/{libraryId}";

        try {
            LibraryResponse response = restTemplate.getForObject(url, LibraryResponse.class, libraryId);
            LOGGER.info("Retrieved library by id: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.info("Getting library failed", e);
            throw new LibraryServiceClientException(e);
        }
    }

    @Override
    public GetLibrariesResponse getLibraries() throws LibraryServiceClientException {
        String url = libraryServiceBaseUrl + "/libraries";

        try {
            GetLibrariesResponse response = restTemplate.getForObject(url, GetLibrariesResponse.class);
            LOGGER.trace("Retrieved all libraries: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.debug("Getting all libraries failed", e);
            throw new LibraryServiceClientException(e);
        }
    }

    @Override
    public LibraryResponse createLibrary(@Valid Library library) throws LibraryServiceClientException {
        String url = libraryServiceBaseUrl + "/libraries";

        try {
            LibraryResponse response = restTemplate.postForObject(url,
                    library,
                    LibraryResponse.class);
            LOGGER.trace("Created library: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.debug("Creating library failed", e);
            throw new LibraryServiceClientException(e);
        }
    }

    @Override
    public void addBook(String libraryId, @Valid Book book) throws LibraryServiceClientException {
        String url = libraryServiceBaseUrl + "/libraries/add-book/{libraryId}";

        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            HttpEntity<Book> httpEntity = new HttpEntity<>(book, httpHeaders);
            restTemplate.exchange(url,
                    HttpMethod.POST,
                    httpEntity,
                    Void.class,
                    libraryId);
            LOGGER.trace("Added book to library");
        } catch (RestClientException e) {
            LOGGER.debug("Adding book to library failed", e);
            throw new LibraryServiceClientException(e);
        }
    }

    @Override
    public GetLibrariesResponse searchByBookTitle(String bookTitle) throws LibraryServiceClientException {
        String url = libraryServiceBaseUrl + "/libraries/search-by-book-title/{bookTitle}";

        try {
            GetLibrariesResponse response = restTemplate.getForObject(url, GetLibrariesResponse.class, bookTitle);
            LOGGER.info("Retrieved library by book title: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.info("Getting library by book failed", e);
            throw new LibraryServiceClientException(e);
        }
    }
}
