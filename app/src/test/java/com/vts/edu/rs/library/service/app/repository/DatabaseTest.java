package com.vts.edu.rs.library.service.app.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DatabaseTest {

    @Autowired
    private LibraryRepository libraryRepository;

    @Test
    public void createLibraryTest() {
        LibraryEntity libraryEntity = new LibraryEntity();
        libraryEntity.setName("Library");
        libraryEntity.setAddress("Address");
        List<BookEntity> books = singletonList(new BookEntity("book-unique-id"));
        libraryEntity.setBooks(books);
        LibraryEntity savedLibraryEntity = libraryRepository.save(libraryEntity);

        assertNotNull(savedLibraryEntity);
        assertEquals(libraryEntity.getName(), savedLibraryEntity.getName());
        assertEquals(libraryEntity.getAddress(), savedLibraryEntity.getAddress());

        assertFalse(savedLibraryEntity.getBooks().isEmpty());
    }
}
