package com.vts.edu.rs.library.service.app.repository;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "book")
public class BookEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name = "bookid", nullable = false)
    private String bookId;

    @ManyToMany(mappedBy = "books", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    List<LibraryEntity> libraries;

    public BookEntity() { }

    public BookEntity(String bookId) {
        this.bookId = bookId;
    }

    public String getId() {
        return id;
    }

    @SuppressWarnings("unused")
    public void setId(String id) {
        this.id = id;
    }

    public String getBookId() {
        return bookId;
    }

    @SuppressWarnings("unused")
    public BookEntity setBookId(String bookId) {
        this.bookId = bookId;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BookEntity)) {
            return false;
        }
        BookEntity that = (BookEntity) o;
        return bookId.equals(that.bookId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId);
    }
}
