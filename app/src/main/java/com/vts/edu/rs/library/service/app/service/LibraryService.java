package com.vts.edu.rs.library.service.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.api.books.model.BookSearchParameters;
import com.vts.edu.rs.books.service.api.books.model.GetBooksResponse;
import com.vts.edu.rs.books.service.client.BooksServiceClient;
import com.vts.edu.rs.books.service.client.BooksServiceClientException;
import com.vts.edu.rs.library.service.api.libraries.model.Book;
import com.vts.edu.rs.library.service.api.libraries.model.Library;
import com.vts.edu.rs.library.service.api.libraries.model.LibraryResponse;
import com.vts.edu.rs.library.service.app.exceptions.NotFoundException;
import com.vts.edu.rs.library.service.app.mappers.LibraryMapper;
import com.vts.edu.rs.library.service.app.repository.BookEntity;
import com.vts.edu.rs.library.service.app.repository.BookRepository;
import com.vts.edu.rs.library.service.app.repository.LibraryEntity;
import com.vts.edu.rs.library.service.app.repository.LibraryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LibraryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryService.class);

    private final LibraryRepository libraryRepository;

    private final BookRepository bookRepository;

    private final LibraryMapper libraryMapper;

    private final BooksServiceClient booksServiceClient;

    private final ObjectMapper objectMapper;

    public LibraryService(LibraryRepository libraryRepository, BookRepository bookRepository, LibraryMapper libraryMapper, BooksServiceClient booksServiceClient, ObjectMapper objectMapper) {
        this.libraryRepository = libraryRepository;
        this.bookRepository = bookRepository;
        this.libraryMapper = libraryMapper;
        this.booksServiceClient = booksServiceClient;
        this.objectMapper = objectMapper;
    }

    @Transactional
    public LibraryResponse createLibrary(Library library) {
        LibraryEntity libraryEntity = libraryMapper.mapLibraryEntity(library);
        LibraryEntity savedLibraryEntity = libraryRepository.save(libraryEntity);
        return libraryMapper.mapLibraryModel(savedLibraryEntity);
    }

    public List<LibraryResponse> getLibraries() {
        Iterable<LibraryEntity> librariesIterator = libraryRepository.findAll();
        List<LibraryResponse> libraries = new ArrayList<>();
        librariesIterator.iterator().forEachRemaining(libraryEntity -> libraries.add(libraryMapper.mapLibraryModel(libraryEntity)));
        return libraries;
    }

    public LibraryResponse getLibraryById(String libraryId) {
        LibraryEntity libraryEntity = libraryRepository.findById(libraryId).orElseThrow(() ->
                new NotFoundException(String.format("Library with id %s does not exist", libraryId)));
        return libraryMapper.mapLibraryModel(libraryEntity);
    }

    public void addBook(String libraryId, Book book) {
        LOGGER.info("Started adding book with id: {} to library with id: {}...", book.getId(), libraryId);
        try {
            LibraryEntity libraryEntity = libraryRepository.findById(libraryId).orElseThrow(() ->
                    new NotFoundException(String.format("Library with id %s does not exist", libraryId)));

            BookEntity bookEntity = bookRepository.findByBookId(book.getId()).orElse(null);

            if (bookEntity == null) {
                LOGGER.info("Book not found into library_service db, calling book-service to find it...");
                BookResponse bookResponse = booksServiceClient.getBookById(book.getId());
                LOGGER.info("Retrieved book from book-service: {}", objectMapper.writeValueAsString(bookResponse));
                bookEntity = new BookEntity();
                bookEntity.setBookId(bookResponse.getId());
                bookEntity = bookRepository.save(bookEntity);
            }

            libraryEntity.addBook(bookEntity);
            libraryRepository.save(libraryEntity);
        } catch (BooksServiceClientException e) {
            LOGGER.info("Failed to get the book with id: {} from book-service", book.getId());
            LOGGER.info("Exception occurred while calling book-service: ", e);
            throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to add book to library");
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error while trying to parse book retrieved from book-service");
        }
    }

    public List<LibraryResponse> searchByBookTitle(String bookTitle) {
        LOGGER.info("Started searching for library that contains the book with title: {}", bookTitle);
        BookResponse book = findBookContainingBookTitle(bookTitle);
        List<LibraryEntity> libraryEntities = libraryRepository.findByBooks_BookId(book.getId());
        return libraryEntities.stream()
                .map(libraryMapper::mapLibraryModel)
                .collect(Collectors.toList());
    }

    private BookResponse findBookContainingBookTitle(String bookTitle) {
        LOGGER.info("Calling book-service to find a book with title: {}", bookTitle);
        try {
            BookSearchParameters bookSearchParams = new BookSearchParameters();
            bookSearchParams.setBookTitle(bookTitle);
            GetBooksResponse booksResponse = booksServiceClient.searchBooks(bookSearchParams);

            if (booksResponse == null || booksResponse.getBooks().isEmpty()) {
                throw new NotFoundException("Failed to find book with name: " + bookTitle);
            }

            return booksResponse.getBooks().get(0);
        } catch (BooksServiceClientException e) {
            LOGGER.info("Exception occurred while calling book-service: ", e);
            LOGGER.info("Failed to find the book with title: {} in book-service", bookTitle);
            throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to find book with name: " + bookTitle);
        }
    }
}
