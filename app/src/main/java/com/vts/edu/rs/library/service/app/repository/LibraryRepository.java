package com.vts.edu.rs.library.service.app.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LibraryRepository extends CrudRepository<LibraryEntity, String> {

    List<LibraryEntity> findByBooks_BookId(String bookId);
}
