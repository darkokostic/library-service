package com.vts.edu.rs.library.service.app.mappers;

import com.vts.edu.rs.library.service.api.libraries.model.Book;
import com.vts.edu.rs.library.service.api.libraries.model.Library;
import com.vts.edu.rs.library.service.api.libraries.model.LibraryResponse;
import com.vts.edu.rs.library.service.app.repository.BookEntity;
import com.vts.edu.rs.library.service.app.repository.LibraryEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.List;

@Mapper(componentModel = "spring")
public interface LibraryMapper {

    @Mapping(source = "libraryEntity.books", target = "numberOfBooks")
    LibraryResponse mapLibraryModel(LibraryEntity libraryEntity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    LibraryEntity mapLibraryEntity(Library library);

    @Mapping(source = "id", target = "bookId")
    @Mapping(target = "id", ignore = true)
    BookEntity mapBookEntity(Book book);

    default OffsetDateTime map(@SuppressWarnings("unused") Instant creationDate) {
        return OffsetDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()),
                ZoneId.systemDefault());
    }

    default Integer map(List<BookEntity> books) {
        if (books != null) {
            return books.size();
        }

        return 0;
    }
}
