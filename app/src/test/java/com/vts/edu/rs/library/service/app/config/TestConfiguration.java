package com.vts.edu.rs.library.service.app.config;

import com.vts.edu.rs.books.service.client.BooksServiceClient;
import com.vts.edu.rs.library.service.app.fakes.BookServiceClientFake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class TestConfiguration {

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;

	@Bean
	public BooksServiceClient booksServiceClient() {
		return new BookServiceClientFake();
	}
}
