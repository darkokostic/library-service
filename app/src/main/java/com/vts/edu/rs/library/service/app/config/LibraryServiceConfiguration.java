package com.vts.edu.rs.library.service.app.config;

import com.vts.edu.rs.books.service.client.BooksServiceClient;
import com.vts.edu.rs.books.service.client.BooksServiceRestClient;
import com.vts.edu.rs.library.service.app.properties.LibraryServiceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

@Configuration
@Profile("production")
public class LibraryServiceConfiguration {

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Autowired
    private LibraryServiceProperties properties;

    @Bean
    public BooksServiceClient booksServiceClient() {
        RestTemplate restTemplate = restTemplateBuilder.build();
        return new BooksServiceRestClient(properties.getBooksServiceBaseUrl(), restTemplate);
    }
}
