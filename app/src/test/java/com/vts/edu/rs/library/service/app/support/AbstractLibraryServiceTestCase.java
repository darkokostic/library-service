package com.vts.edu.rs.library.service.app.support;

import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.library.service.api.libraries.model.Library;
import com.vts.edu.rs.library.service.api.libraries.model.LibraryResponse;
import com.vts.edu.rs.library.service.app.fakes.BookServiceClientFake;
import com.vts.edu.rs.library.service.app.repository.BookEntity;
import com.vts.edu.rs.library.service.app.repository.LibraryEntity;
import com.vts.edu.rs.library.service.app.repository.LibraryRepository;
import com.vts.edu.rs.library.service.app.service.LibraryService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = AbstractLibraryServiceTestCase.Initializer.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
public abstract class AbstractLibraryServiceTestCase {

	public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			TestPropertyValues.of(
				"libraryservice.booksServiceBaseUrl=not-set"
			).applyTo(configurableApplicationContext.getEnvironment());
		}
	}

	@Autowired
	protected LibraryRepository libraryRepository;

	@Autowired
	protected LibraryService libraryService;

	@Autowired
	protected BookServiceClientFake bookServiceClient;

	@Before
	public void setup() {
		libraryRepository.deleteAll();
		bookServiceClient.clear();
	}

	@SuppressWarnings("SameParameterValue")
	protected LibraryEntity saveLibraryEntity(String name, String address, List<BookEntity> books) {
		LibraryEntity libraryEntity = new LibraryEntity();
		libraryEntity.setName(name);
		libraryEntity.setAddress(address);
		libraryEntity.setBooks(books);
		return libraryRepository.save(libraryEntity);
	}

	protected BookResponse saveBookToBookService(@SuppressWarnings("SameParameterValue") String bookId) {
		BookResponse book = new BookResponse().id(bookId);
		bookServiceClient.addBook(book);
		return book;
	}

	@SuppressWarnings("SameParameterValue")
	protected BookResponse saveBookToBookService(String bookId, String bookTitle) {
		BookResponse book = new BookResponse().id(bookId).title(bookTitle);
		bookServiceClient.addBook(book);
		return book;
	}

	protected void assertLibrary(LibraryEntity libraryEntity, LibraryResponse libraryResponse) {
		assertNotNull(libraryResponse);
		assertEquals(libraryEntity.getName(), libraryResponse.getName());
		assertEquals(libraryEntity.getAddress(), libraryResponse.getAddress());
		assertEquals(Integer.valueOf(1), libraryResponse.getNumberOfBooks());
	}

	protected void assertLibrary(Library library, LibraryResponse libraryResponse) {
		assertNotNull(libraryResponse);
		assertEquals(library.getName(), libraryResponse.getName());
		assertEquals(library.getAddress(), libraryResponse.getAddress());
	}

}
