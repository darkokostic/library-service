package com.vts.edu.rs.library.service.app.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("libraryservice")
public class LibraryServiceProperties {

    private String booksServiceBaseUrl;

    public String getBooksServiceBaseUrl() {
        return booksServiceBaseUrl;
    }

    public void setBooksServiceBaseUrl(String booksServiceBaseUrl) {
        this.booksServiceBaseUrl = booksServiceBaseUrl;
    }
}
