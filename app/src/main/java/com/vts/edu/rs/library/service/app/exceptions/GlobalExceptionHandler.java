package com.vts.edu.rs.library.service.app.exceptions;

import com.vts.edu.rs.library.service.api.libraries.model.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ErrorResponse notFoundException(NotFoundException e) {
        LOGGER.debug("Resource not found", e);
        return createErrorResponse(e);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    public ErrorResponse badRequestException(BadRequestException e) {
        LOGGER.debug("Bad request received", e);
        return createErrorResponse(e);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ConflictException.class)
    public ErrorResponse conflictException(ConflictException e) {
        LOGGER.debug("Conflict received", e);
        return createErrorResponse(e);
    }

    private ErrorResponse createErrorResponse(Exception e) {
        return new ErrorResponse().message(e.getMessage());
    }
}